import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StoreManagementService {

  state$: Observable<Map<String, any>>;
  private readonly _state$: BehaviorSubject<Map<String, any>>;

  constructor(initialState: Map<string, any>) { 
    this._state$ = new BehaviorSubject(initialState);
  }

  getState = (key: string) => {
    return this._state$.getValue().get(key);
  }

  setState(nextState: Map<string, any>): void {
    const merged = new Map([...this._state$.getValue(), ...nextState]);
    this._state$.next(merged);
  }
}
