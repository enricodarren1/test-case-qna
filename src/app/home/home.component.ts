import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StoreManagementService } from '../services/store-management.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  questionFormGroup: FormGroup;
  questionList: any = [];
  selectedMenu = 'question';

  constructor(
    private readonly fb: FormBuilder,
    private readonly storeService: StoreManagementService
  ) { }

  ngOnInit(): void {
    this.createForm();

    this.getAllQuestionList();
  }

  createForm() {
    this.questionFormGroup = this.fb.group({
      question: new FormControl('', Validators.required),
      name: new FormControl('', Validators.required)
    })
  }

  getAllQuestionList() {
    this.questionList = this.storeService.getState('questionList') ? this.storeService.getState('questionList') : [];
  }

  addQuestion() {
    const newData: any = {
      id: this.questionList.length > 0 ? this.questionList.length + 1 : 1,
      question: this.questionFormGroup.value.question,
      name: this.questionFormGroup.value.name,
      likes: 0
    }
    this.questionList.push(newData);
    this.storeService.setState(new Map<string, any>().set('questionList', this.questionList));
    // RESET FORM VALUE
    this.questionFormGroup.reset();
    // FOR REFRESH DATA
    this.getAllQuestionList();
  }

}
