import { Component, OnInit, Input } from '@angular/core';
import { from, of } from 'rxjs';
import { find, map } from 'rxjs/operators';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StoreManagementService } from '../services/store-management.service';

@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.scss']
})
export class QuestionListComponent implements OnInit {

  @Input() allQuestion: any;
  @Input() isAnswered: any;

  answerFormGroup: FormGroup;
  selected: any;

  constructor(private readonly fb: FormBuilder, private readonly storeService: StoreManagementService) { }

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
    this.answerFormGroup = this.fb.group({
      answer: new FormControl('', Validators.required)
    })
  }

  onLikes(selectedData: any) {
    const subsAllQuestion = from(this.allQuestion).pipe(find((data: any) => selectedData.id === data.id), map(dataMap => dataMap.likes = dataMap.likes + 1));
    subsAllQuestion.subscribe(res => {
      this.allQuestion.map((dataMap: any) => {
        if (dataMap.id === selectedData.id) {
          dataMap.likes = res;
          return dataMap
        }
      })

    })
    this.storeService.setState(new Map<string, any>().set('questionList', this.allQuestion));
  }

  selectedData(data: any) {
    this.selected = data;
  }

  onAnswer() {
    
    this.allQuestion.map((dataMap: any) => {
      if (dataMap.id === this.selected.id) {
        dataMap.answer = this.answerFormGroup.value.answer;
      }
    })
    this.storeService.setState(new Map<string, any>().set('questionList', this.allQuestion));
    this.answerFormGroup.reset();
  }

}
